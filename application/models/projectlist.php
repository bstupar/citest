<?php
/**
 * @author  : Bosko Stupar <bosko.stupar@gmail.com>
 * @date    : 5/24/13
 * Location : projectlist.php
 */

if ( !defined('BASEPATH')) exit ('No direct script access');

class Projectlist extends CI_Model {
    // model for projects and tasks

    /**
     * Adds new project to the database
     *
     * @param $project_data array
     * @return bool
     */
    public function add_project($project_data)
    {
        $project = $this->db->insert('projects', $project_data);
        return $project;
    }

    /**
     * Update project
     *
     * @param $id
     * @param $project_data array
     * @return bool
     */
    public function update_project($id,$project_data)
    {
        $project = $this->db->update('projects', $project_data, array('id' => $id));
        return $project;
    }

    /**
     * Get project name based on it's ID (used for view file)
     *
     * @param $id
     * @return mixed array
     */
    public function project_name($id)
    {
        $this->db->select('project_name')->from('projects')->where('id', $id)->limit(1);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0];
    }

    /**
     * Get list of projects
     *
     * @return mixed
     */
    public function list_projects()
    {
        // TODO: Uraditi paginaciju ! i prikazivanje u zavisnosti da li je sudo il customer
        $this->db->select('projects.id, projects.project_name,
                            users.first_name, users.last_name,
                            status.status_name, status.status_css');
        $this->db->from('projects');
        $this->db->join('users', 'projects.user_id = users.id');
        $this->db->join('status', 'projects.project_status = status.id');
        $this->db->limit(10);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    /**
     * Get project details based on ID
     *
     * @param $id
     * @return mixed array
     */
    public function get_project($id)
    {
        $this->db->select('projects.id, projects.project_name, projects.project_status, projects.project_desc, status.status_name, status.status_css');
        $this->db->from('projects');
        $this->db->join('status', 'projects.project_status = status.id');
        $this->db->where('projects.id', $id);
        $this->db->limit(1);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    /**
     * Get list of tasks for specific project
     *
     * @param $id
     * @return mixed array
     */
    public function list_tasks($id)
    {
        $this->db->select('tasks.id, tasks.task_name, status.status_name, status.status_css');
        $this->db->from('tasks');
        $this->db->join('status', 'tasks.task_status = status.id');
        $this->db->where('tasks.project_id', $id);
        $this->db->limit(10);

        $query = $this->db->get();
        $result = $query->result_array();

        if (empty($result)) {
            return false;
        } else {
            return $result;
        }
    }

    /**
     * Create new task w/ provided data
     *
     * @param $task_data array
     * @return bool
     */
    public function add_task($task_data)
    {
        $task = $this->db->insert('tasks', $task_data);
        return $task;
    }

}