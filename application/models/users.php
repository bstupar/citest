<?php
/**
 * @author  : Bosko Stupar <bosko.stupar@gmail.com>
 * @date    : 5/24/13
 * Location : user.php
 */

if ( !defined('BASEPATH')) exit ('No direct script access');

class Users extends CI_Model {

    /**
     * Register new user with provided data
     *
     * @param $data mixed
     * @return bool
     */
    function createUser($data)
    {
        $user = $this->db->insert('users', $data);

        if ($user) {
            $data['loggedin'] = true;
            $this->session->set_userdata($data);
            return true;
        } else {
            return false;
        }
    }


    /**
     * Get user data from database used for auth and storing in session
     *
     * @param $email string
     * @return mixed
     */
    private function getUserData($email)
    {
        $this->db->select()->from('users')->where('user_email', $email)->limit(1);
        $query = $this->db->get();
        // we need to return an array for later on modification !
        foreach ($query->result_array() as $row) {
            $user = $row;
        }
        return $user;
    }

    /**
     * Authentication method used to login user
     *
     * @param $user_data mixed
     * @return bool
     */
    function auth($user_data)
    {
        $user = $this->getUserData($user_data['user_email']);
        if (Hash::check($user_data['user_pass'], $user['user_pass'])) {
            $user['loggedin'] = true;
            $this->session->set_userdata($user);
            return true;
        } else {
            return false;
        }
    }
}