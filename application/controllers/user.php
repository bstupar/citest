<?php
/**
 * @author  : Bosko Stupar <bosko.stupar@gmail.com>
 * @date    : 5/24/13
 * Location : ./application/controllers/user.php
 */

if ( !defined('BASEPATH')) exit ('No direct script access');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('users', 'user');
        $this->load->library('hash');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $user = $this->session->all_userdata();
        dd($user);
    }

    /**
     * Registracija usera,provera validacije forme
     * login nakon uspesne registracije i redirect na project page
     */
    public function register()
    {
        if (!$this->input->post()) {
            $this->load->view('register');

        } else {

            if ($this->form_validation->run('register')) {
                // form validacija uspesna, napraviti usera i redirectovati ga
                $reg_data = array(
                    'first_name'    => $this->input->post('first_name'),
                    'last_name'     => $this->input->post('last_name'),
                    'user_email'    => $this->input->post('user_email'),
                    'user_pass'     => $this->hash->make($this->input->post('user_pass'))
                );

                $user = $this->user->createUser($reg_data);

                if ($user == true) {
                    // successful registration and login, redirect to main page
                    redirect('projects', 'refresh');

                } else {
                    // registration failed
                    $data['reg_error'] = 'Registration failed, please try again.';
                    $this->load->view('register',$data);

                }

            } else {
                // validation failed
                $this->load->view('register');
            }
        }
    }

    public function login()
    {

        if (check_login() == true) {
            redirect('projects', 'refresh');

        } else {

            if (!$this->input->post()) {
                $this->load->view('login');

            } else {

                // do validation, auth user if possible, redirect him to projects page
                if ($this->form_validation->run('login')) {
                    $user_data = array(
                        'user_email'    => $this->input->post('user_email'),
                        'user_pass'     => $this->input->post('user_pass')
                    );
                    $user = $this->user->auth($user_data);

                    if ($user) {
                        redirect('projects', 'refresh');
                    } else {
                        $data['login_error'] = 'Authentication failed!';
                        $this->load->view('login', $data);
                    }

                } else {

                    //validation failed
                    $this->load->view('login');
                }
            }
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('user/login', 'refresh');
    }

    public function manage($id)
    {
        // edit user if youre admin
    }

    public function changepass()
    {
        // change password for current user
    }

}