<?php
/**
 * @author  : Bosko Stupar <bosko.stupar@gmail.com>
 * @date    : 5/24/13
 * Location : ./application/controllers/projects.php
 */

if ( !defined('BASEPATH')) edit ('No direct script access');

class Projects extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('projectlist', 'project');
        $logged_in = check_login();
        if ($logged_in != true OR !isset($logged_in)) {
            redirect('user/login', 'refresh');
        }
    }

    /**
     * Index controller for displaying main project page
     */
    public function index()
    {
            $data = array(
                'project_list' => $this->project->list_projects()
            );
            $this->load->view('index', $data);
    }

    public function view($slug)
    {
        $id = slug_id($slug);
        $project = $this->project->get_project($id);
        $data = array(
            'user' => $this->session->userdata('first_name') . ' ' . $this->session->userdata('last_name'),
            'project' => $project[0],
            'task_list' => $this->project->list_tasks($id)
        );
        $this->load->view('project', $data);
    }
    public function add()
    {

        if (!$this->input->post()) {
            // form is not submitted
            $data['user'] = $this->session->userdata('first_name') . ' ' . $this->session->userdata('last_name');
            $this->load->view('project_add', $data);
        } else {
            // form submitted insert in database
            $project_data = array(
                'project_name'      => $this->input->post('project_name'),
                'project_status'    => '1',
                'user_id'           => $this->session->userdata('id'),
                'project_desc'      => $this->input->post('project_desc')
            );
            $project = $this->project->add_project($project_data);

            if ($project) {
                // set success message in session that is after that passed to the index view file
                $this->session->set_flashdata('project_add', 'Project successfully created!');
                redirect('projects', 'refresh');
            } else {
                dd('nije napravljen project');
            }
        }
    }

    public function edit($slug)
    {
        $id = slug_id($slug);
        if (!$this->input->post()) {
            // form is not submitted
            $project = $this->project->get_project($id);
            $data = array(
                'project' => $project[0]
            );
            $this->load->view('editproject', $data);
        } else {
            // form submitted
            $project_data = array(
                'project_name' => $this->db->escape_str($this->input->post('project_name')),
                'project_status' => $this->input->post('project_status'),
                'project_desc' => $this->input->post('project_desc')
            );
            //dd($project_data);
            $project = $this->project->update_project($id, $project_data);
            //dd($project);
            if ($project) {
                $this->session->set_flashdata('project_add', 'Project successfully updated!');
                $slug = slug($id, $project_data['project_name']);
                redirect('projects/'.$slug,'refresh');
            } else {
                dd('update failed !');
            }
        }
    }
    public function test($slug)
    {
        dd($this->session->userdata('id'));
    }

    /*****************************************
     *                                       *
     *   Tasks part of the controller        *
     *                                       *
     *****************************************/


    public function add_task($slug)
    {
        $id = slug_id($slug);
        if (!$this->input->post()) {
            // form is not submitted
            $prev_name = $this->project->project_name($id);
            $data = array(
                'prev_link' => base_url('projects/' . $slug),
                'prev_name' => $prev_name['project_name']
            );
            $this->load->view('addtask', $data);
        } else {
            $task_data = array(
                'project_id' => $id,
                'task_name' => $this->input->post('task_name'),
                'task_desc' => $this->input->post('task_desc'),
                'task_status' => 1
            );
            $task = $this->project->add_task($task_data);
            //dd($task);
            if ($task) {
                $this->session->set_flashdata('project_add', 'Taks successfully created!');
                redirect(base_url('projects/' . $slug), 'refresh');
            } else {
                dd('failed to create task!');
            }

        }
    }

}