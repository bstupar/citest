﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>TaskList | Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Bosko Stupar">

        <!-- Le styles -->
        <link href="<? echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
        <link href="<? echo base_url() ?>assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="<? echo base_url() ?>assets/js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- Container starts -->

        <div class="container">
            <?php if (isset($login_error)) { ?>

                <div class="alert">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Woops.. </strong> <?php echo $login_error ?>
                </div>
            <?php } ?>
            <?php if (validation_errors()) { ?>

                <div class="alert">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Woops.. </strong> <?php echo validation_errors()?>
                </div>
            <?php } ?>
            <div class="modal">
                <div class="well">

                    <!-- Form starts -->

                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active in" id="login">
                            <form class="form-horizontal" action='<?php echo base_url('user/login') ?>' method="post">
                                <fieldset>
                                    <div id="legend">
                                        <legend class="">Login</legend>
                                    </div>    

                                    <!-- E-mail -->

                                    <div class="control-group">                  
                                        <label class="control-label" for="email">E-mail</label>
                                        <div class="controls">
                                            <input type="text" id="user_email" name="user_email" placeholder="Email" class="input-xlarge" required>
                                        </div>
                                    </div>

                                    <!-- Password-->

                                    <div class="control-group"> 
                                        <label class="control-label" for="password">Password</label>
                                        <div class="controls">
                                            <input type="password" id="user_pass" name="user_pass" placeholder="Password" class="input-xlarge" required>
                                        </div>
                                    </div>

                                    <!-- Button -->

                                    <div class="control-group">              
                                        <div class="controls">
                                            <button class="btn btn-success">Login</button> or <a href="<? echo base_url('user/register') ?>"> Register here</a>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>