﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>TaskList | Projects</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="<? echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
        <link href="<? echo base_url() ?>assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="<? echo base_url() ?>assets/js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- Container starts -->

        <div class="container">

            <!-- Header starts -->

            <div>
                <ul class="breadcrumb">
                    <li><h3><a href="<?php echo base_url('projects')?>">Projects list</a> / <?php echo $project['project_name']?></h3></li>
                </ul>
            </div>

            <!-- Description starts -->

            <div class="well">
                <?php ?>
                <dl class="dl-horizontal">
                    <dt>Owner:</dt>
                    <dd><? echo $user ?></dd>
                    <dt>Project Name:</dt>
                    <dd><?php echo $project['project_name'] ?></dd>
                    <dt>Project Status:</dt>
                    <dd><span class="label label-<?php echo $project['status_css']?>"><?php echo $project['status_name'] ?></span></dd>
                    <dt>Description:</dt>
                    <dd><?php echo str_replace("\n", "<br />", $project['project_desc']); ?></dd>
                </dl>
                <a href="<? echo current_url() . '/addtask'?>"><button class="btn" type="button">Add task</button></a>
            </div>

            <!-- Table starts -->
            <?php if (isset($task_list) AND $task_list != false) { ?>
            <table class="table table-hover">

                <!-- Table header starts -->

                <thead>
                    <tr>
                        <th>#</th>
                        <th>Task</th>
                        <th>Status</th>
                        <th>Options</th>
                    </tr>
                </thead>

                <!-- Table body starts -->

                <tbody>
                <?php foreach($task_list as $task) {
                    ?>
                    <tr>
                        <td><? echo $task['id'] ?></td>
                        <td><a href="<?php echo base_url('tasks/' . $task['id'])?>"><?php echo $task['task_name']?></a></td>
                        <td><span class="label label-<?php echo $task['status_css']?>"><?php echo $task['status_name'] ?></span></td>
                        <td>
                            <div class="btn-group">
                                <a href="<?php echo base_url('tasks/edit/' . $task['id']) ?>"><button type="button"><span class="icon-pencil"></span></button></a>
                                <a href="<?php echo base_url('tasks/delete/'. $task['id']) ?>"><button type="button"><span class="icon-trash"></span></button></a>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <?php } ?>
            <hr>

            <!-- Uncompleted Tasks -->

            <h3>Uncompleted tasks</h3>
            <div class="row-fluid">

                <div class="span4">
                    <h4><a href="task.php">Task 1</a></h4>
                    <p>This is the Task 1. Enter a lot of tasks here to have task-full day with more and more tasks. Because tasks are good. Tasks make you improve. </p>
                    <p><span class="label label-success">Success</span></p>
                </div>

                <div class="span4">
                    <h4><a href="task.php">Task 2</a></h4>
                    <p>This is the Task 2. Enter a lot of tasks here to have task-full day with more and more tasks. Because tasks are good. Tasks make you improve. </p>
                    <p><span class="label label-warning">Warning</span></p>
                </div>

                <div class="span4">
                    <h4><a href="task.php">Task 3</a></h4>
                    <p>This is the Task 3. Enter a lot of tasks here to have task-full day with more and more tasks. Because tasks are good. Tasks make you improve. </p>
                    <p><span class="label label-important">Important</span></p>
                </div>
            </div>
        </div>
        <!-- Placed at the end so the page loads faster -->
        <script type="text/javascript" src="<? echo base_url() ?>assets/js/jquery-2.0.2.min.js"></script>
        <script type="text/javascript" src="<? echo base_url() ?>assets/js/bootstrap.min.js"></script>
    </body>
</html>