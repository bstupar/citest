<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>TaskList  | Add Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<? echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<? echo base_url() ?>assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<? echo base_url() ?>assets/js/html5shiv.js"></script>
    <![endif]-->
</head>
<body>

<!-- Container starts -->

<div class="container">

    <!-- Project editor starts -->

    <div class="row-fluid">
        <?php if (validation_errors()) { ?>

            <div class="alert">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Woops.. </strong> <?php echo validation_errors()?>
            </div>
        <?php } ?>
        <form class="form-horizontal" action="<?php echo base_url('projects/add') ?>" method="post">
            <fieldset>

                <div class="control-group">
                    <label class="control-label" for="author">Author</label>
                    <div class="controls">
                        <input type="text" id="author" name="author" value="<?php echo $user ?>" disabled>
                    </div>
                </div>
                <!-- Project name -->

                <div class="control-group">
                    <label class="control-label"  for="project_name">Project name</label>
                    <div class="controls">
                        <input type="text" id="project_name" name="project_name" placeholder="Project Name .." class="input-xlarge" required>
                    </div>
                </div>

                <!-- Project description -->

                <div class="control-group">
                    <label class="control-label" for="project_desc">Description</label>
                    <div class="controls">
                        <textarea class="field span9" id="project_desc" name="project_desc" rows="6" inputtype="text" placeholder="Project Description .." required></textarea>
                    </div>
                </div>

                <!-- Submit or delete -->

                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-success">Done</button>
                        <button class="btn btn-delete">Delete</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
</body>
</html>