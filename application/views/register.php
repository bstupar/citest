<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>TaskList |   Register</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="<? echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
        <link href="<? echo base_url() ?>css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="<? echo base_url() ?>/assets/js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- Container starts -->

        <div class="container">
            <?php if (validation_errors()) { ?>

                <div class="alert">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Woops.. </strong> <?php echo validation_errors()?>
                </div>
            <?php } ?>
            <div class="modal">


                <div class="well">

                    <!-- Form starts -->

                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active in" id="login">
                            <form class="form-horizontal" action='<? echo base_url('user/register')?>' method="POST">
                                <fieldset>
                                    <div id="legend">
                                        <legend class="">Register</legend>
                                    </div>    

                                    <!-- First name -->

                                    <div class="control-group">
                                        <label class="control-label" for="first_name">First Name</label>
                                        <div class="controls">
                                            <input type="text" id="first_name" name="first_name" placeholder="First name" class="input-xlarge" required>
                                        </div>
                                    </div>

                                    <!-- Last name -->

                                    <div class="control-group">
                                        <label class="control-label" for="last_name">Last Name</label>
                                        <div class="controls">
                                            <input type="text" id="last_name" name="last_name" placeholder="Last name" class="input-xlarge" required>
                                        </div>
                                    </div>

                                    <!-- Email -->

                                    <div class="control-group">               
                                        <label class="control-label" for="user_email">E-mail</label>
                                        <div class="controls">
                                            <input type="text" id="user_email" name="user_email" placeholder="Email..." class="input-xlarge" required>
                                        </div>
                                    </div>

                                    <!-- Password -->

                                    <div class="control-group">
                                        <label class="control-label" for="user_pass">Password</label>
                                        <div class="controls">
                                            <input type="password" id="user_pass" name="user_pass" placeholder="Enter password" class="input-xlarge" required>
                                        </div>
                                    </div>

                                    <!-- Confirm password-->

                                    <div class="control-group">
                                        <label class="control-label" for="user_passconf">Confirm password</label>
                                        <div class="controls">
                                            <input type="password" id="user_passconf" name="user_passconf" placeholder="Confirm password" class="input-xlarge" required>
                                        </div>
                                    </div>

                                    <!-- Button -->

                                    <div class="control-group">
                                        <div class="controls">
                                            <button class="btn btn-success">Register</button> or <a href="<? echo base_url('user/login') ?>">Login here</a>
                                        </div>
                                    </div>
                                </fieldset>                 
                            </form>                
                        </div>         
                    </div>
                </div>
            </div>
        </div>
    <!-- JavaScript Load -->
    <script type="text/javascript" src="<? echo base_url()?>assets/js/bootstrap.js"
    </body>
</html>