﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Ovde ide naziv firme kojim uzimamo pare</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<? echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<? echo base_url() ?>assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<? echo base_url() ?>assets/assets/js/html5shiv.js"></script>
    <![endif]-->
  </head>
  <body>
    	
   	<!-- Container starts -->

    <div class="container">

      <!-- Header starts -->

      <div>
        <ul class="breadcrumb">
          <li><h3>Projects list</h3></li>
        </ul>
      </div>
    
      <!-- Description starts -->

      <div class="well">
        <h5>These are the projects you're currently working on. To view the tasks click on the Project in the table below.</h5>
          <a href="<? echo base_url('projects/add')?>"><button class="btn" type="button">Add project</button></a>
      </div>
       
      <!-- Table header starts -->
      <?php if (isset($project_list)) { ?>
      <table class="table table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>Project</th>
            <th>Status</th>
            <th>Author</th>
            <th>Options</th>
          </tr>
        </thead>

        <!-- Table body starts -->
        
        <tbody>
        <?php foreach($project_list as $project) {?>
          <tr>
            <td><?php echo $project['id']; ?></td>
            <td><a href="<?php echo slug_url('projects/', $project['id'], $project['project_name']) ?>"><?php echo $project['project_name']; ?></a></td>
            <td><span class="label label-<?php echo $project['status_css']; ?>"><?php echo $project['status_name']; ?></span></td>
            <td><?php echo $project['first_name'] . ' ' . $project['last_name']; ?></td>
            <td class="btn-group">
              <a href="<?php echo slug_url('projects/edit/', $project['id'], $project['project_name']) ?>"><button type="button"><span class="icon-pencil"></span></button></a>
              <a href="<?php echo base_url('projects/delete/' . $project['id']) ?>"><button type="button"><span class="icon-trash"></span></button></a>
            </td>              
          </tr>
       <?php } ?>
        </tbody>
      </table>
      <?php } ?>
    </div>
    <!-- Placed on the end so the page loads faster -->
    <script type="text/javascript" src="<? echo base_url() ?>assets/js/bootstrap.min.js"></script>
  </body>
</html>
