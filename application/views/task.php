<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Task name</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- Container starts -->

        <div class="container">

            <!-- Header starts -->

            <div class="row">
                <ul class="breadcrumb">
                    <li>
                        <h3><a href="project.php">Project Name</a> / Task Name</h3>
                    </li>
                </ul>

                <!-- Description starts -->

                <div class="well">
                    <dl class="dl-horizontal">
                        <dt>Author:</dt>
                        <dd>Bosko</dd>
                        <dt>Description:</dt>
                        <dd>This is the task description.</dd>
                    </dl>

                </div>

                <!-- Comments starts -->

                <div class="span11">
                    <h3>Comments</h3>
                    <dl class="dl-horizontal">
                        <dt>Author</dt>
                        <dd>This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.</dd>
                        <dt>Author</dt>
                        <dd>This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.</dd>
                        <dt>Author</dt>
                        <dd>This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.This be post.</dd>
                    </dl>
                </div>

                <!-- Quick reply starts -->

                <div class="span8">
                    <h3>Quick Reply</h3>
                    <textarea class="field span8" id="textarea" name="textarea" rows="6" inputtype="text" placeholder="Enter your comment..." required></textarea>
                    <button class="btn">Post</button>
                </div>
            </div>
        </div>
    </body>
</html>