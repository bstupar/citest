<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add task</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
</head>
<body>

<!-- Container starts -->

<div class="container">

    <!-- Header starts -->

    <div>
        <ul class="breadcrumb">
            <li>
                <h3><a href="<?php echo base_url('projects/') ?>">Projects</a> / <a href="<?php echo $prev_link ?>"><?php echo $prev_name ?></a> / Task name</h3>
            </li>
        </ul>
    </div>

    <!-- Task editor form start -->

    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo $prev_link . '/addtask'?>" method="post">
            <fieldset>

                <!-- Task name -->

                <div class="control-group">
                    <label class="control-label"  for="task_name">Task name</label>
                    <div class="controls">
                        <input type="text" id="task" name="task_name" placeholder="Enter task name ..." class="input-xlarge"required>
                    </div>
                </div>

                <!-- Task description -->

                <div class="control-group">
                    <label class="control-label" for="task_desc">Description</label>
                    <div class="controls">
                        <textarea class="field span9" id="textarea" name="task_desc" rows="6" inputtype="text" placeholder="Task Description ..." required></textarea>
                    </div>
                </div>

                <!-- Submit or delete -->

                <div class="control-group">
                    <div class="controls">
                        <button class="btn btn-success">Done</button>
                        <button class="btn btn-delete">Delete</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<script type="text/javascript" src="<? echo base_url() ?>assets/js/jquery-2.0.2.min.js"></script>
<script type="text/javascript" src="<? echo base_url() ?>assets/js/bootstrap.min.js"></script>
</body>
</html>