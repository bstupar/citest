﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Edit task</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="../assets/js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- Container starts -->

        <div class="container">

            <!-- Header starts -->

            <div>
                <ul class="breadcrumb">
                    <li>
                        <h3><a href="index.php">Tasks editor</a> / Task name</h3>
                    </li>
                </ul>
            </div>  

            <!-- Task editor form start -->

            <div class="row-fluid">
                <form class="form-horizontal" action="task.php" method="post">
                    <fieldset>

                        <!-- Task name -->

                        <div class="control-group">
                            <label class="control-label"  for="task">Task name</label>
                            <div class="controls">
                                <input type="text" id="task" name="task" placeholder="Change the task name here..." class="input-xlarge"required>
                            </div>
                        </div>

                        <!-- Task description -->

                        <div class="control-group">
                            <label class="control-label" for="description">Description</label>
                            <div class="controls">
                                <textarea class="field span9" id="textarea" name="textarea" rows="6" inputtype="text" placeholder="Change the description here..." required></textarea>
                            </div>
                        </div>

                        <!-- Task progress-->

                        <div class="control-group">
                            <label class="control-label" for="progress">Task progress</label>
                            <div class="controls">
                                <select class="span2" name="progress" id="progress">
                                    <option value="01" selected>Success</option>
                                    <option value="02">Warning</option>
                                    <option value="03">Important</option>
                                    <option value="04">Default</option>
                                    <option value="05">Info</option>
                                </select>
                            </div>
                        </div>

                        <!-- Submit or delete -->

                        <div class="control-group">
                            <div class="controls">
                                <button class="btn btn-success">Done</button>
                                <button class="btn btn-delete">Delete</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </body>
</html>