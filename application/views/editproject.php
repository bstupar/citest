<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Edit project</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="<? echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
        <link href="<? echo base_url() ?>assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="<? echo base_url() ?>assets/js/html5shiv.js"></script>
        <![endif]-->
    </head>
    <body>

        <!-- Container starts -->

        <div class="container">

            <!-- Header starts -->

            <div>
                <ul class="breadcrumb">
                    <li><h3><a href="<?php echo slug_url('projects/', $project['id'], $project['project_name'])?>">Project editor</a> / <?php echo $project['project_name'] ?></h3></li>
                </ul>
            </div>  

            <!-- Project editor starts -->

            <div class="row-fluid">
                <form class="form-horizontal" action="<?php echo slug_url('projects/edit/', $project['id'], $project['project_name']) ?>" method="post">
                    <fieldset>

                        <!-- Project name -->

                        <div class="control-group">
                            <label class="control-label"  for="project_name">Project name</label>
                            <div class="controls">
                                <input type="text" id="project_name" name="project_name" placeholder="Project Name ..." class="input-xlarge" value="<?php echo $project['project_name']?>" required>
                            </div>
                        </div>

                        <!-- Project description -->

                        <div class="control-group">
                            <label class="control-label" for="project_desc">Description</label>
                            <div class="controls">
                                <textarea class="field span9" id="project_desc" name="project_desc" rows="6" inputtype="text" placeholder="Project Description ..." required><?php echo $project['project_desc']?></textarea>
                            </div>
                        </div>

                        <!-- Project progress-->

                        <div class="control-group">
                            <label class="control-label" for="project_status">Project progress</label>
                            <div class="controls">
                                <select class="span3" name="project_status" id="project_status">
                                    <option value="1" <? if ($project['project_status'] === '1') echo "selected" ?>>Open</option>
                                    <option value="2" <? if ($project['project_status'] === '2') echo "selected" ?>>Finished</option>
                                    <option value="3" <? if ($project['project_status'] === '3') echo "selected" ?>>In Progress</option>
                                    <option value="4" <? if ($project['project_status'] === '4') echo "selected" ?>>Declined</option>
                                </select>
                            </div>
                        </div>


                        <!-- Submit or delete -->

                        <div class="control-group">
                            <div class="controls">
                                <button class="btn btn-success">Done</button>
                                <button class="btn btn-delete">Delete</button>
                            </div>
                        </div>
                    </fieldset>
                </form>

            </div>
        </div>

    </body>
</html>