<?php
/**
 * @author  : Bosko Stupar <bosko.stupar@gmail.com>
 * @date    : 5/28/13
 * Location : ./application/helpers/myhelp_helper.php
 */


/**
 * Dump the given valuen and kill the script
 *
 * @param $value mixed
 * @return void
 */
function dd($value)
{
    die(var_dump($value));
}

/**
 * Checks if user is logged in
 *
 * @return bool
 */
function check_login()
{
    $CI =& get_instance();
    $logged_in = $CI->session->userdata('loggedin');
    if (!isset($logged_in) OR $logged_in !== true) {
        return false;
    } else {
        return true;
    }
}

/**
 * Checks if user is admin or not
 *
 * @return bool
 */
function check_role()
{
    $ci =& get_instance();
    $role = $ci->session->userdata('user_role');
    if ($role === 'true') {
        return true;
    } if ($role === 'false') {
        return false;
    }
}

/**
 *  Create url slug link with base_url() function that's used in view files
 *
 * @param $path     controller/method or route path with added slash at the end /
 * @param $id       $id that's passed to the method
 * @param $name     name of project/task or w/e that's used to build slug
 * @return string
 */
function slug_url($path,$id,$name)
{
    $pattern = array(" ", "!", "@", "#", "$", "%", "^", "&", "*", "`", ":", "<", ">", "[", "]", "{", "}", "\"", "+", ";",
                    "=", "\\", "?", "'", ".", ",", "~", "|", "/");
    //change all to lower case
    $str = strtolower($name);
    //add dashes
    $str = str_replace($pattern,'-', $str);
    //append ID
    $str = $id . '-' . $str;
    $uri = base_url($path . $str);
    return $uri;
}

/**
 *  Create slug from passed parameters
 *
 * @param $id       id that's passed to the method
 * @param $name     name of project/task that's used to build slug
 * @return string
 */
function slug($id, $name)
{
    $pattern = array(" ", "!", "@", "#", "$", "%", "^", "&", "*", "`", ":", "<", ">", "[", "]", "{", "}", "\"", "+", ";",
                    "=", "\\", "?", "'", ".", ",", "~", "|", "/");
    $str = strtolower($name);
    $str = str_replace($pattern, '-', $str);
    $str = $id . '-' . $str;
    return $str;
}

/**
 * Get the ID from the slug
 *
 * @param $slug
 * @return string
 */
function slug_id($slug)
{
    $parts = explode('-',$slug);
    $id = $parts[0];
    return $id;
}