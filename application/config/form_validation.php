<?php
/**
 * @author  : Bosko Stupar <bosko.stupar@gmail.com>
 * @date    : 5/24/13
 * Location : form_validation.php
 */

$config = array(
    'register' => array(
        array(
            'field' => 'first_name',
            'label' => 'First Name',
            'rules' => 'required|alpha'
        ),
        array(
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'required|alpha'
        ),
        array(
            'field' => 'user_email',
            'label' => 'Email',
            'rules' => 'required|valid_email|is_unique[users.user_email]'
        ),
        array(
            'field' => 'user_pass',
            'label' => 'Password',
            'rules' => 'required|min_length[6]'
        ),
        array(
            'field' => 'user_passconf',
            'label' => 'Confirm Password',
            'rules' => 'required|matches[user_pass]'
        )
    ),
    'login' =>  array(
        array(
            'field' => 'user_email',
            'label' => 'Email',
            'rules' => 'required|valid_email|xss_clean'
        ),
        array(
            'field' => 'user_pass',
            'label' => 'Password',
            'rules' => 'required|xss_clean'
        )
    )


);